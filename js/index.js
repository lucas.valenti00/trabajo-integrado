$(document).ready(function(){
    $.ajax({
      url: "https://api.openweathermap.org/data/2.5/weather?q=CABA&appid=9907da3a3df67eebeb31254d4d2115b4&units=metric",
      type: "GET",
      dataType: "jsonp",
      success: function(data){
        console.log(data);
        var weatherIcon = "http://openweathermap.org/img/w/" + data.weather[0].icon + ".png";
        var weatherTemp = Math.round(data.main.temp) + "°C";
        var weatherDesc = data.weather[0].description;
        $('#weather-image').html('<img src="' + weatherIcon + '">');
        $('#weather-temperature').html(weatherTemp);
        $('#weather-description').html(weatherDesc);
      }
    });
  });
  


document.addEventListener("DOMContentLoaded", () => {
    const $boton = document.querySelector("#btnCrearPdf");
    $boton.addEventListener("click", () => {
         const $elementoParaConvertir = document.querySelector('#confirmar');
        html2pdf()
            .set({
                margin: 1,
                filename: 'registro.pdf',
                image: {
                    type: 'png',
                    quality: 0.98
                },
                html2canvas: {
                    scale: 3,
                    letterRendering: true,
                    scrollY: 0
                },
                jsPDF: {
                    unit: "in",
                    format: "a3",
                    orientation: 'portrait'
                }
            })
            .from($elementoParaConvertir)
            .save()
            .catch(err => console.log(err));
    });
});

$(document).ready(function() {
    class Formulario {
        nombre;
        apellido;
        email;
        ciudad;
        provincia;
        git;
        proyecto;
        nacimiento;
        favorito;
        nofavorito;
    }

    var f = new Formulario();
    (() => {
        'use strict'
        var bloque1 = $('#primerVista');
        var bloque2 = $('#segundaVista');
        const forms = $('.needs-validation')

        Array.from(forms).forEach(form => {
            form.addEventListener('submit', event => {
                if (form.checkValidity() == false) {
                    event.stopPropagation(); 
                    event.preventDefault();
                    form.classList.add('was-validated');
                } else {
                    form.classList.add('was-validated');
                    event.stopPropagation();
                    event.preventDefault();
                    switch (form.id) {
                        case "primerVista":
                            bloque1.addClass('d-none');
                            bloque2.removeClass('d-none');
                            break;
                        case "segundaVista":
                            LlenarClaseFormulario();
                            break;
                            case "formContacto":
                                break;
                    }

                }

            }, false)
        })




    })()
    
    function LlenarClaseFormulario() {
        f.nombre = $('#nombre').val();
        f.apellido = $('#apellido').val();
        f.email = $('#email').val();
        f.ciudad = $('#ciudad').val();
        f.provincia = $('#provincia').val();
        f.nacimiento = $('#nacimiento').val();
        f.favorito = $("#favorito option:selected").val();
        f.nofavorito = $("#nofavorito option:selected").val();
        f.git = $('#git').val();
        f.proyecto = $('#proyecto').val();

        $('#nombreConfirmar').val(f.nombre);
        $('#apellidoConfirmar').val(f.apellido);
        $('#ciudadConfirmar').val(f.ciudad);
        $('#provinciaConfirmar').val(f.provincia);
        $('#emailConfirmar').val(f.email);
        $('#nacimientoConfirmar').val(f.nacimiento);
        $('#favoritoConfirmar').val(f.favorito);
        $('#noFavoritoConfirmar').val(f.nofavorito);
        $('#gitConfirmar').val('@' + f.git);
        $('#proyectoConfirmar').val(f.proyecto);
        $('#segundaVista').addClass('d-none');
        $('#confirmar').removeClass('d-none'); 
    }

    $("#btnSegundaVistaAtras").click(function() {
        $('#primerVista').removeClass('d-none');
        $('#segundaVista').addClass('d-none');
    });

    $("#btnVolverConfirma").click(function() {
        $('#primerVista').removeClass('d-none');
        $('#confirmar').addClass('d-none');
    });

    $("#btnConfirmarDatos").click(function() {
        $('#contenedorJson').removeClass('d-none');
        $('#json').html(JSON.stringify(f, null, 4));
    });
});

